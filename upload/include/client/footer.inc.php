	<footer class="container">
		<div class="row">
			<div class="twelvecol last">
				<p>osTicket theme by <a href="http://jaypick.me" title="Jay Pick - Freelance Web Designer" target="_blank">Jay Pick</a>.</p>
				<a href="http://osticket.com" class="osLogo"><img src="./images/osticket-powered.gif" alt="Powered by osTicket"></a>
			</div>
		</div>
	</footer>

</body>
</html>
